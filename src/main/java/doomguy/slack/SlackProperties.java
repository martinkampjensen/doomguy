package doomguy.slack;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "doomguy.slack")
record SlackProperties(String appToken, String botToken) {

}
