package doomguy.slack;

import com.slack.api.bolt.App;
import com.slack.api.bolt.AppConfig;
import com.slack.api.bolt.socket_mode.SocketModeApp;
import com.slack.api.socket_mode.SocketModeClient.Backend;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(SlackProperties.class)
class SlackConfiguration {

    @Bean
    AppConfig slackAppConfig(SlackProperties properties) {

        return AppConfig.builder()
                .singleTeamBotToken(properties.botToken())
                .build();
    }

    @Bean
    App slackApp(AppConfig appConfig) {

        return new App(appConfig);
    }

    @Bean(destroyMethod = "stop")
    SocketModeApp slackSocketModeApp(SlackProperties properties, App app) throws Exception {

        SocketModeApp socketModeApp = new SocketModeApp(properties.appToken(), Backend.JavaWebSocket, app);
        socketModeApp.startAsync();
        return socketModeApp;
    }
}
