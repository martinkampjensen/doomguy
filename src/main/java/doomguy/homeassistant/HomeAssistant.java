package doomguy.homeassistant;

import com.slack.api.bolt.App;
import com.slack.api.bolt.response.Response;
import com.slack.api.model.event.AppMentionEvent;
import java.time.Duration;
import java.util.Objects;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class HomeAssistant {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeAssistant.class);

    private final WebClient homeAssistantWebClient;

    private final App slackApp;

    public HomeAssistant(WebClient homeAssistantWebClient, App slackApp) {

        this.homeAssistantWebClient = Objects.requireNonNull(homeAssistantWebClient);
        this.slackApp = Objects.requireNonNull(slackApp);
    }

    @PostConstruct
    private void postConstruct() {

        slackApp.event(AppMentionEvent.class, (event, context) -> {
            context.ack();
            context.say("<@" + event.getEvent().getUser() + "> Home Assistant API is " + (isRunning() ? "" : "NOT ") + "running");
            return Response.ok();
        });
    }

    private boolean isRunning() {

        try {
            String json = homeAssistantWebClient.get()
                    .uri("/")
                    .retrieve()
                    .bodyToMono(String.class)
                    .block(Duration.ofSeconds(2));
            return json != null && json.contains("API running.");
        } catch (RuntimeException ex) {
            LOGGER.error("Error when calling isRunning", ex);
            return false;
        }
    }
}
