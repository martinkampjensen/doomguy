package doomguy.homeassistant;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "doomguy.homeassistant")
record HomeAssistantProperties(String apiToken, String baseUrl) {

}
